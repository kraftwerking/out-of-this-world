//
//  KRAFTWERKINGSpaceDataViewController.h
//  Out Of This World
//
//  Created by RJ Militante on 1/27/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KRAFTWERKINGSpaceObject.h";

@interface KRAFTWERKINGSpaceDataViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) KRAFTWERKINGSpaceObject *spaceObject;

@end
