//
//  KRAFTWERKINGAddSpaceObjectViewController.h
//  Out Of This World
//
//  Created by RJ Militante on 1/28/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KRAFTWERKINGSpaceObject.h"

@protocol KRAFTWERKINGAddSpaceObjectViewControllerDelegate <NSObject>

@required
-(void)addSpaceObject:(KRAFTWERKINGSpaceObject *)spaceObject;
-(void)didCancel;


@end

@interface KRAFTWERKINGAddSpaceObjectViewController : UIViewController

@property (weak, nonatomic) id <KRAFTWERKINGAddSpaceObjectViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *nickNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *diameterTextField;
@property (strong, nonatomic) IBOutlet UITextField *temperatureTextField;
@property (strong, nonatomic) IBOutlet UITextField *numberOfMoonsTextField;
@property (strong, nonatomic) IBOutlet UITextField *interestingFactTextField;
- (IBAction)cancelButtonPressed:(UIButton *)sender;
- (IBAction)addButtonPressed:(UIButton *)sender;



@end
