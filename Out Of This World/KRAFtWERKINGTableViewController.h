//
//  KRAFtWERKINGTableViewController.h
//  Out Of This World
//
//  Created by RJ Militante on 1/22/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KRAFTWERKINGAddSpaceObjectViewController.h"

@interface KRAFtWERKINGTableViewController : UITableViewController<KRAFTWERKINGAddSpaceObjectViewControllerDelegate>

@property (strong, nonatomic) NSMutableArray *planets;
@property (strong, nonatomic) NSMutableArray *addedSpaceObjects;

@end
