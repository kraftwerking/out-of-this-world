//
//  KRAFTWERKINGSpaceObject.m
//  Out Of This World
//
//  Created by RJ Militante on 1/23/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "KRAFTWERKINGSpaceObject.h"
#import "AstronomicalData.h";

@implementation KRAFTWERKINGSpaceObject

-(id) init
{
    self = [self initWithData:nil andImage:nil];
    return self;
    
}

-(id) initWithData:(NSDictionary *)data andImage:(UIImage *)image
{
    self = [super init];
    
    self.name = data[PLANET_NAME];
    self.gravitationalForce  = [data[PLANET_GRAVITY] floatValue];
    self.diameter  = [data[PLANET_DIAMETER] floatValue];
    self.yearLength  = [data[PLANET_YEAR_LENGTH] floatValue];
    self.dayLength  = [data[PLANET_DAY_LENGTH] floatValue];
    self.temperature  = [data[PLANET_TEMPERATURE] floatValue];
    self.numberOfMoons  = [data[PLANET_NUMBER_OF_MOONS] intValue];
    self.nickName = data[PLANET_NICKNAME];
    self.interestingFact = data[PLANET_INTERESTING_FACT];
    
    self.spaceImage  = image;
    
    return self;
}

@end
