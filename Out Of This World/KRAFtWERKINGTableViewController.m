//
//  KRAFtWERKINGTableViewController.m
//  Out Of This World
//
//  Created by RJ Militante on 1/22/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "KRAFtWERKINGTableViewController.h"
#import "AstronomicalData.h"
#import "KRAFTWERKINGSpaceObject.h"
#import <UIKit/UIKit.h>
#import "KRAFTWERKINGSpaceImageViewController.h"
#import "KRAFTWERKINGSpaceObject.h"
#import "KRAFTWERKINGSpaceDataViewController.h"

@interface KRAFtWERKINGTableViewController ()

@end

@implementation KRAFtWERKINGTableViewController

#pragma lazy instantiation of properties
-(NSMutableArray *)planets{
    if(!_planets){
        _planets = [[NSMutableArray alloc] init];
    }
    return _planets;
}

-(NSMutableArray *)addedSpaceObjects{
    if(!_addedSpaceObjects){
        _addedSpaceObjects = [[NSMutableArray alloc] init];
    }
    return _addedSpaceObjects;
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //self.planets = [[NSMutableArray alloc] init];
    
    for (NSMutableDictionary *planetData in [AstronomicalData allKnownPlanets])
    {
        NSString *imageName = [NSString stringWithFormat:@"%@.jpg", planetData[PLANET_NAME]];
        KRAFTWERKINGSpaceObject *planet = [[KRAFTWERKINGSpaceObject alloc] initWithData: planetData andImage:[UIImage imageNamed:imageName]];
        [self.planets addObject:planet];
    }
    
    
//    NSString *planet1 = @"Mercury";
//    NSString *planet2 = @"Venus";
//    NSString *planet3 = @"Earth";
//    NSString *planet4 = @"Mars";
//    NSString *planet5 = @"Jupiter";
//    NSString *planet6 = @"Saturn";
//    NSString *planet7 = @"Uranus";
//    NSString *planet8 = @"Neptune";
    
//    self.planets = [[NSMutableArray alloc] initWithObjects:planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, nil];

//    [self.planets addObject:planet1];
//    [self.planets addObject:planet2];
//    [self.planets addObject:planet3];
//    [self.planets addObject:planet4];
//    [self.planets addObject:planet5];
//    [self.planets addObject:planet6];
//    [self.planets addObject:planet7];
//    [self.planets addObject:planet8];
    
//    NSMutableDictionary *myDictionary = [[NSMutableDictionary alloc] init];
//    NSString *firstColor = @"red";
//    [myDictionary setObject:firstColor forKey:@"firetruck color"];
//    
//    [myDictionary setObject:@"blue" forKey:@"ocean color"];
//    [myDictionary setObject:@"yellow" forKey:@"star color"];
//    NSLog(@"%@", myDictionary);
//    NSString *blueString = [myDictionary objectForKey:@"ocean color"];
//    NSLog(@"%@", blueString);
    
    NSNumber *myNumber = [NSNumber numberWithInt:5];
    NSLog(@"%@",myNumber);
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //NSLog(@"%@", sender);
    if([sender isKindOfClass:[UITableViewCell class]])
    {
        if([segue.destinationViewController isKindOfClass:[KRAFTWERKINGSpaceImageViewController class]])
        {
            KRAFTWERKINGSpaceImageViewController *nextViewController = segue.destinationViewController;
            NSIndexPath *path = [self.tableView indexPathForCell:sender];
            KRAFTWERKINGSpaceObject *selectedObject;
            if(path.section == 0){
                selectedObject = self.planets[path.row];
            } else if (path.section == 1){
                selectedObject = self.addedSpaceObjects[path.row];
            }
            
            nextViewController.spaceObject = selectedObject;
        }
        
    }
        /* The prepareForSegue method is called right before the viewController transition occurs. Notice that we do introspection to ensure that the Segue is being triggered by the proper sender. In this case we pass in the NSIndexPath of the accessory button pressed. We then confirm that the destination ViewController is the OWSpaceDataViewController. Finally, we create a variable named targetViewController that points to our destination ViewController. Determine the indexPath of the selected cell and use that indexPath to access a OWSpaceObject in our planet array. Finally set the property spaceobject of the variable targetViewController equal to the selected object. */
        
        if ([sender isKindOfClass:[NSIndexPath class]])
        {
            if ([segue.destinationViewController isKindOfClass:[KRAFTWERKINGSpaceDataViewController class]])
            {
                KRAFTWERKINGSpaceDataViewController *targetViewController = segue.destinationViewController;
                NSIndexPath *path = sender;
                KRAFTWERKINGSpaceObject *selectedObject;
                if(path.section == 0){
                    selectedObject = self.planets[path.row];
                } else if (path.section == 1){
                    selectedObject = self.addedSpaceObjects[path.row];
                }

                targetViewController.spaceObject = selectedObject;
            }
        }
        
        /* If the destination ViewController is the addSpaceObjectVC we create a variable that points to the OWAddSpaceObjectViewController instance. With this instance we can set the delegate property so that the OWAddSpaceObjectViewController can call the methods defined in its' protocol. */
        if ([segue.destinationViewController isKindOfClass:[KRAFTWERKINGAddSpaceObjectViewController class]]){
            KRAFTWERKINGAddSpaceObjectViewController *addSpaceObjectVC = segue.destinationViewController;
            addSpaceObjectVC.delegate = self;
        }
        
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - KRAFTWERKINGAddSpaceObjectViewController Delegate

/* Delegate method defined in the OWAddSpaceObjectViewController Protocol. This method dismisses the instance of the OWAddSpaceObjectViewController and presents the OWOuterSpaceTableViewController*/
-(void)didCancel
{
    NSLog(@"didCancel");
    [self dismissViewControllerAnimated:YES completion:nil];
}

/* Delegate method defined in the OWAddSpaceObjectViewController Protocol. First we determine if the property of addedSpaceObjects has a value. If it does not we assign memory and initialize the object. Next we add the spaceObject passed into this method as a parameter. Then we dismiss the instance of the OWAddSpaceObjectViewController and presents the OWOuterSpaceTableViewController. Finally, with our updated array we update the TableView. */
-(void)addSpaceObject:(KRAFTWERKINGSpaceObject *)spaceObject
{
//    if (!self.addedSpaceObjects){
//        self.addedSpaceObjects = [[NSMutableArray alloc] init];
//    }
    [self.addedSpaceObjects addObject:spaceObject];
    
    NSLog(@"addSpaceObject");
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.tableView reloadData];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    if([self.addedSpaceObjects count]){
        return 2;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    if(section == 1){
        return [self.addedSpaceObjects count];
    } else {
        return [self.planets count];
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    // Configure the cell...
    if(indexPath.section == 1) {
        //use spaceObject to customize your cell
        KRAFTWERKINGSpaceObject *planet = [self.addedSpaceObjects objectAtIndex:indexPath.row];
        cell.textLabel.text = planet.name;
        cell.detailTextLabel.text = planet.nickName;
        cell.imageView.image = planet.spaceImage;
        
    } else {
        KRAFTWERKINGSpaceObject *planet = [self.planets objectAtIndex:indexPath.row];
        cell.textLabel.text = planet.name;
        cell.detailTextLabel.text = planet.nickName;
        cell.imageView.image = planet.spaceImage;
    }

    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"Accessory button is working correctly %i", indexPath. row);
    [self performSegueWithIdentifier:@"push to space data" sender:indexPath];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
