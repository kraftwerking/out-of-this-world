//
//  KRAFTWERKINGSpaceImageViewController.h
//  Out Of This World
//
//  Created by RJ Militante on 1/26/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KRAFTWERKINGSpaceObject.h"

@interface KRAFTWERKINGSpaceImageViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) KRAFTWERKINGSpaceObject *spaceObject;

@end
