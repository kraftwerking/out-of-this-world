//
//  KRAFTWERKINGAddSpaceObjectViewController.m
//  Out Of This World
//
//  Created by RJ Militante on 1/28/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "KRAFTWERKINGAddSpaceObjectViewController.h"

@interface KRAFTWERKINGAddSpaceObjectViewController ()

@end

@implementation KRAFTWERKINGAddSpaceObjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage *orionImage = [UIImage imageNamed:@"Orion.jpg"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:orionImage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/* Call the method defined in the protocol. The method should be implemented in the OWOuterSpaceTableViewController. */
- (IBAction)cancelButtonPressed:(UIButton *)sender
{
    [self.delegate didCancel];
}

/* Call the method defined in the protocol. The method should be implemented in the OWOuterSpaceTableViewController. For the parameter of OWSpaceObject use the helper method returnNewSpaceObject.*/
- (IBAction)addButtonPressed:(UIButton *)sender
{
    KRAFTWERKINGSpaceObject *newSpaceObject = [self returnNewSpaceObject];
    [self.delegate addSpaceObject:newSpaceObject];
}

/* Method returns an OWSpaceObject using the information supplied from the textFields. Use a default image the EinsteinRing*/
-(KRAFTWERKINGSpaceObject *)returnNewSpaceObject
{
    KRAFTWERKINGSpaceObject *addedSpaceObject = [[KRAFTWERKINGSpaceObject alloc] init];
    addedSpaceObject.name = self.nameTextField.text;
    addedSpaceObject.nickName = self.nickNameTextField.text;
    addedSpaceObject.diameter = [self.diameterTextField.text floatValue];
    addedSpaceObject.temperature = [self.temperatureTextField.text floatValue];
    addedSpaceObject.numberOfMoons = [self.numberOfMoonsTextField.text intValue];
    addedSpaceObject.interestingFact = self.interestingFactTextField.text;
    addedSpaceObject.spaceImage = [UIImage imageNamed:@"EinsteinRing.jpg"];
    
    return addedSpaceObject;
}
@end
